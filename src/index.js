const appDiv = document.getElementById('appDiv');
const button = document.getElementById('generateBtn');
button.id = 'generateBtn';
appDiv.append(button);

const data = {
  subjects: ['The sun ', 'The moon ', 'The truth ', 'Your life ', 'Chocolate ', 'Love '],
  verbs: ['is ', 'seems ', 'looks ', 'becomes ', 'stays ', 'appears ', 'remains ', 'will be '],
  adjectives: ['blue. ', 'green. ', 'sad. ', 'happy. ', 'green. ', 'lonely. ', 'friendly. ', 'needy. ', 'creepy. ', 'beautiful. ', 'kind. ', 'true. '],
};
function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}
function takeOne(array) {
  const randInt = getRandomInt(array.length);
  return array[randInt];
}

function generateLine() {
  let line = '';
  line += takeOne(data.subjects);
  line += takeOne(data.verbs);
  line += takeOne(data.adjectives);
  return line;
}
function generateHaiku(div) {
  const newDiv = document.createElement('div');
  const first = document.createElement('p');
  first.innerText = generateLine();
  const second = document.createElement('p');
  second.innerText = generateLine();
  const third = document.createElement('p');
  third.innerText = generateLine();
  const closeBtn = document.createElement('button');
  closeBtn.innerText = 'X';
  closeBtn.style.border = 'none';
  closeBtn.style.cssFloat = 'right';
  closeBtn.style.backgroundColor = 'white';
  newDiv.append(closeBtn);
  closeBtn.addEventListener('click', (event) => {
    event.target.parentNode.className += ' displayNone';
  });
  newDiv.append(first, second, third);
  newDiv.className += 'haiku';
  div.append(newDiv);
}
button.addEventListener('click', () => {
  generateHaiku(appDiv);
});
